var number1 = parseFloat(prompt("Введіть перше число:"));
var number2 = parseFloat(prompt("Введіть друге число:"));

var operation = prompt("Введіть математичну операцію (+, -, *, /):");

function calculate(num1, num2, operator) {
  var result;

  switch (operator) {
    case "+":
      result = num1 + num2;
      break;
    case "-":
      result = num1 - num2;
      break;
    case "*":
      result = num1 * num2;
      break;
    case "/":
      result = num1 / num2;
      break;
    default:
      console.log("Непідтримувана операція!");
      return;
  }

  return result;
}

var result = calculate(number1, number2, operation);
console.log("Результат: " + result);

// Функції потрібні для того щоб розділяти складні завдання на більш дрібні і прості
// З метою надання функції змінних, які вона може використовувати для виконання дій або обчислень
// Return выкористовуєтся для того щоб повертати значення функції назад до місця
